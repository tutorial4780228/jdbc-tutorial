package com.amigoscode.domain.movie;

import java.util.List;
import java.util.Optional;

public interface MovieDao {
    List<Movie> selectMovies();
    int insertMovie(Movie movie);
    int deleteMovie(int id);
    Optional<Movie> selectMovieById(int id);
    void updateMovie(int id, Movie modifiedMovie);
    boolean movieAlreadyExists(Movie movieCandidate);
}
