package com.amigoscode.domain.movie;

import com.amigoscode.domain.actor.Actor;

import java.time.LocalDate;
import java.util.List;

public record Movie(
        Integer id,
        String name,
        List<Actor> actors,
        LocalDate releaseDate
) {
    public void addActor(Actor actor) {
        actors.add(actor);
    }
}