package com.amigoscode.domain.movie;

import com.amigoscode.domain.exception.EntityAlreadyExistsException;
import com.amigoscode.domain.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {
    private final MovieDao movieDao;

    public MovieService(MovieDao movieDao) {
        this.movieDao = movieDao;
    }

    public List<Movie> getMovies() {
        return movieDao.selectMovies();
    }

    public void addNewMovie(Movie movie) throws EntityAlreadyExistsException {
        if (movieDao.movieAlreadyExists(movie)) {
            throw new EntityAlreadyExistsException(
                    String.format("Movie '%s' released in %s already exists", movie.name(), movie.releaseDate())
            );
        }

        int result = movieDao.insertMovie(movie);
        if (result != 1) {
            throw new IllegalStateException("oops something went wrong");
        }
    }

    public void deleteMovie(Integer id) throws EntityNotFoundException {
        Optional<Movie> movies = movieDao.selectMovieById(id);
        movies.ifPresentOrElse(movie -> {
            int result = movieDao.deleteMovie(id);
            if (result != 1) {
                throw new IllegalStateException("oops could not delete movie");
            }
        }, () -> {
            throw new EntityNotFoundException(String.format("Movie with id %s not found", id));
        });
    }

    public Movie getMovie(int id) {
        return movieDao.selectMovieById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Movie with id %s not found", id)));
    }

    public void updateMovie(Integer id, Movie modifiedMovie) throws EntityNotFoundException {
        getMovie(id);
        movieDao.updateMovie(id, modifiedMovie);
    }
}
