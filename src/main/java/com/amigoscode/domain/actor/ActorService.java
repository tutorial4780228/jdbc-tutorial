package com.amigoscode.domain.actor;

import com.amigoscode.domain.exception.EntityAlreadyExistsException;
import org.springframework.stereotype.Service;

@Service
public class ActorService {
    private final ActorDao actorDao;

    public ActorService(ActorDao actorDao) {
        this.actorDao = actorDao;
    }

    public void addActor(Actor actor) throws EntityAlreadyExistsException {
        if (actorDao.actorAlreadyExists(actor)) {
            throw new EntityAlreadyExistsException(
                    String.format("Actor '%s' for film #%d already exists", actor.name(), actor.movieId())
            );
        }
        actorDao.addActor(actor);
    }
}
