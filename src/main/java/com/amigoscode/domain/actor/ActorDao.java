package com.amigoscode.domain.actor;

public interface ActorDao {
    void addActor(Actor actor);
    boolean actorAlreadyExists(Actor actor);
}
