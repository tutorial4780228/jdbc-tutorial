package com.amigoscode.domain.actor;

public record Actor(
        Integer id,
        String name,
        Integer movieId
) {}
