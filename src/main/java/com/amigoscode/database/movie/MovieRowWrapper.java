package com.amigoscode.database.movie;

import com.amigoscode.domain.movie.Movie;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

public class MovieRowWrapper implements RowMapper<Movie> {
    public Movie mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Movie(
            resultSet.getInt("m.id"),
            resultSet.getString("m.name"),
            List.of(),
            LocalDate.parse(resultSet.getString("m.release_date")
            )
        );
    }
}
