package com.amigoscode.database.movie;

import com.amigoscode.domain.movie.Movie;
import com.amigoscode.domain.movie.MovieDao;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class MovieDataAccessService implements MovieDao {
    private final JdbcTemplate jdbcTemplate;

    public MovieDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Movie> selectMovies() {
        String sql = """
            SELECT m.id, m.name, m.release_date,
                   a.id, a.name, a.movie
            FROM movie m
            LEFT JOIN actor a ON a.movie = m.id;
        """;

        return jdbcTemplate.query(sql, new MovieResultSetExtractor());
    }

    @Override
    public int insertMovie(Movie movie) {
        String sql = """
            INSERT INTO movie (name, release_date)
            VALUES(?, ?);
        """;
        return jdbcTemplate.update(sql, movie.name(), movie.releaseDate());
    }

    @Override
    public int deleteMovie(int id) {
        String sql = """
            DELETE FROM movie
            WHERE id = ?
        """;

        return jdbcTemplate.update(sql, id);
    }

    @Override
    public Optional<Movie> selectMovieById(int id) {
        String sql = """
            SELECT m.id, m.name, m.release_date,
                   a.id, a.name, a.movie
            FROM movie m
            LEFT JOIN actor a ON a.movie = m.id
            WHERE m.id = ?;
        """;

        return Objects.requireNonNull(jdbcTemplate.query(sql, new MovieResultSetExtractor(), id))
                .stream()
                .findFirst();
    }

    @Override
    public void updateMovie(int id, Movie modifiedMovie) {
        String sql = """
            UPDATE movie
            SET name = ?, release_date = ?
            WHERE id = ?
        """;

        jdbcTemplate.update(
                sql,
                modifiedMovie.name(),
                modifiedMovie.releaseDate(),
                id
        );
    }

    public boolean movieAlreadyExists(Movie movieCandidate) {
        String sql = """
            SELECT COUNT(*) AS count
            FROM movie
            WHERE name = ? AND release_date = ?;
        """;

        return jdbcTemplate.query(sql, (ResultSet resultSet, int i) ->
            resultSet.getInt("count") > 0,
        movieCandidate.name(), movieCandidate.releaseDate()).get(0);
    }
}
