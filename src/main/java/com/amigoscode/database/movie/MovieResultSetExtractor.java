package com.amigoscode.database.movie;

import com.amigoscode.domain.actor.Actor;
import com.amigoscode.domain.movie.Movie;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MovieResultSetExtractor implements ResultSetExtractor<List<Movie>> {
    private Map<Integer, Movie> uniqueMovies = new HashMap<>();

    public List<Movie> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        while (resultSet.next()) {
            if (movieAlreadyAdded(resultSet) && rowHasActor(resultSet)) {
                addActorToMovie(resultSet);
            } else {
                addMovie(resultSet);
                if (rowHasActor(resultSet)) {
                    addActorToMovie(resultSet);
                }
            }
        }

        return uniqueMovies
                .values()
                .stream()
                .toList();
    }

    private boolean rowHasActor(ResultSet resultSet) throws SQLException {
        return resultSet.getInt("a.id") != 0;
    }

    private void addMovie(ResultSet resultSet) throws SQLException {
        Integer movieId = resultSet.getInt("m.id");
        Movie newMovie = new Movie(
            movieId,
            resultSet.getString("m.name"),
            new ArrayList<>(),
            LocalDate.parse(resultSet.getString("m.release_date"))
        );
        uniqueMovies.put(movieId, newMovie);
    }

    private boolean movieAlreadyAdded(ResultSet resultSet) throws SQLException {
        return uniqueMovies.containsKey(resultSet.getInt("m.id"));
    }

    private void addActorToMovie(ResultSet resultSet) throws SQLException {
        Actor actor = new Actor(
            resultSet.getInt("a.id"),
            resultSet.getString("a.name"),
            resultSet.getInt("a.movie")
        );
        Integer movieId = resultSet.getInt("m.id");
        Movie existingMovie = uniqueMovies.get(movieId);
        existingMovie.addActor(actor);
    }
}
