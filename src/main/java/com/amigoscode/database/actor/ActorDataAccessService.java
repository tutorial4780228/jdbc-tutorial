package com.amigoscode.database.actor;

import com.amigoscode.domain.actor.Actor;
import com.amigoscode.domain.actor.ActorDao;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;

@Repository
public class ActorDataAccessService implements ActorDao {
    private JdbcTemplate jdbcTemplate;

    public ActorDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void addActor(Actor actor) {
        String sql = """
            INSERT INTO actor(name, movie)
            VALUES(?, ?);
        """;

        jdbcTemplate.update(sql, actor.name(), actor.movieId());
    }

    public boolean actorAlreadyExists(Actor actor) {
        String sql = """
            SELECT COUNT(*) as count
            FROM actor
            WHERE name = ? AND movie = ?;
        """;

        return jdbcTemplate.query(sql, (ResultSet resultSet, int i) ->
                resultSet.getInt("count") > 0,
            actor.name(), actor.movieId()).get(0);
    }
}
