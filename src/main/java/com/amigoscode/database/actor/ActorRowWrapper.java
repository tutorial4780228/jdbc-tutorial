package com.amigoscode.database.actor;

import com.amigoscode.domain.actor.Actor;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ActorRowWrapper implements RowMapper<Actor> {
    public Actor mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Actor(
            resultSet.getInt("a.id"),
            resultSet.getString("a.name"),
            resultSet.getInt("a.movie")
        );
    }
}
