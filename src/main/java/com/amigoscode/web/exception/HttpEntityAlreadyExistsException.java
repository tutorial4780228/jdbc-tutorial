package com.amigoscode.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class HttpEntityAlreadyExistsException extends RuntimeException {
    public HttpEntityAlreadyExistsException(String message) {
        super(message);
    }
}
