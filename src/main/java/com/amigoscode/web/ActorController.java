package com.amigoscode.web;

import com.amigoscode.domain.actor.Actor;
import com.amigoscode.domain.actor.ActorService;
import com.amigoscode.domain.exception.EntityAlreadyExistsException;
import com.amigoscode.web.exception.HttpEntityAlreadyExistsException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/actors")
public class ActorController {
    private final ActorService actorService;

    public ActorController(ActorService actorService) {
        this.actorService = actorService;
    }

    @PostMapping
    public void addActor(@RequestBody Actor actor) {
        try {
            actorService.addActor(actor);
        } catch (EntityAlreadyExistsException exception) {
            throw new HttpEntityAlreadyExistsException(exception.getMessage());
        }
    }
}
