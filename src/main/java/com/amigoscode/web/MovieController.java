package com.amigoscode.web;

import com.amigoscode.domain.exception.EntityAlreadyExistsException;
import com.amigoscode.domain.exception.EntityNotFoundException;
import com.amigoscode.domain.movie.Movie;
import com.amigoscode.domain.movie.MovieService;
import com.amigoscode.web.exception.HttpEntityAlreadyExistsException;
import com.amigoscode.web.exception.HttpNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/movies")
public class MovieController {
    private final MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping
    public List<Movie> listMovies() {
        return movieService.getMovies();
    }

    @GetMapping("{id}")
    public Movie getMovieId(@PathVariable("id") Integer id) {
        try {
            return movieService.getMovie(id);
        } catch (EntityNotFoundException exception) {
            throw new HttpNotFoundException(exception.getMessage());
        }
    }

    @PostMapping
    public void addMovie(@RequestBody Movie movie) {
        try {
            movieService.addNewMovie(movie);
        } catch (EntityAlreadyExistsException exception) {
            throw new HttpEntityAlreadyExistsException(exception.getMessage());
        }
    }

    @DeleteMapping("{id}")
    public void deleteMovie(@PathVariable("id") Integer id) {
        try {
            movieService.deleteMovie(id);
        } catch (EntityNotFoundException exception) {
            throw new HttpNotFoundException(exception.getMessage());
        }
    }

   @PutMapping("{id}")
    public void updateMovie(@PathVariable("id") Integer id, @RequestBody Movie movie) {
        try {
            movieService.updateMovie(id, movie);
        } catch (EntityNotFoundException exception) {
            throw new HttpNotFoundException(exception.getMessage());
        }
   }
}
