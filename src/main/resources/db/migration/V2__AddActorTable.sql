CREATE TABLE actor (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    movie INTEGER REFERENCES movie(id),
    UNIQUE(name, movie)
)